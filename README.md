# Building of image from Dockerfile and pushing to Gitlab Registry

## My gitlab-ci.yml file
```
# two stages of pipeline
stages:
  - build
  - test

# create variables 
variables:
  JENKINS_IMAGE: "$CI_REGISTRY_IMAGE:1.0.${CI_PIPELINE_ID}"
  LATEST_IMAGE: $CI_REGISTRY_IMAGE:latest
  INT: '80'
  EXT: '80'
  CONTAINER_NAME: 'jenkins_husak'

# Login to Gitlab Registry in each stage before executing main script
before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

# First stage is building from my local Dockerfile. Specifying runner performs by writing tags  
build_image:
  stage: build
  when: manual
  script:
    - docker build -t $JENKINS_IMAGE .
    - docker push $JENKINS_IMAGE
  tags:
    - jenkins

# Second stage is testing of running container with curl and then pushing one container with version tag and second with latest tag
test_and_push:
  stage: test
  when: manual
  script:
    - docker pull $JENKINS_IMAGE 
    - docker run -d --name $CONTAINER_NAME -p $INT:$EXT $JENKINS_IMAGE 
    - sleep 60
    - curl http://localhost
    - docker stop $CONTAINER_NAME 
    - docker rm $CONTAINER_NAME 
    - docker push $JENKINS_IMAGE
    - docker tag $JENKINS_IMAGE $LATEST_IMAGE
    - docker push $LATEST_IMAGE
  tags:
    - jenkins
```

***


